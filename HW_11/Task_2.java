package HW_11;
import java.util.*;
public class Task_2 {
    public static void main(String[] args) {
       Scanner in = new Scanner(System.in);
       System.out.println("Введите тип фигура где: 1 - круг, 2 - прямоугольник , 3 - треугольник ");
       int figure = in.nextInt();
       double []a = new double[figure];

       for (int i =0;i < figure;i++) {
           System.out.println("Введите длинну стороны");
           a[i] = in.nextDouble();

       }
        double result = area(figure, a);
        System.out.println(result);
    }
    public static double area(int figure,double a[]) {//Метод возвращающий площадь той или иной фигуры
        if (figure == 1) {
            double res = areaCircle(a);
            return res;
        }
        else if (figure == 2) {
            double res = areaRectangle(a);
            return res;
        }
        else  {
            double res = areaTriangle(a);
            return res;
        }
    }
    public static double areaCircle(double a[]) {//Площадь круга
        double area_Circle = (a[0]*a[0])/4*3.14;
        return area_Circle;
    }

    public static double areaRectangle(double a[]) {//Площадь прямоугольика
        double area_Rectangle = a[0] * a[1];
        return area_Rectangle;
    }

    public static double areaTriangle(double a[]) {//Площадь треугольника
        double P = (a[0] + a[1] + a[2]) / 2;
        double S = Math.sqrt(P * (P - a[0]) * (P - a[1]) * (P - a[2]));
        return S;
    }
}
