package HW_11;
import java.util.Scanner;
public class Task_1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        int [] a = new int[size];
        for (int i = 0; i < size; i++) {
            int x = in.nextInt();
            a[i] = x;
        }
        double res = avg(a);
        System.out.println(res);
    }
    public static double avg(int a[]) {//Метод для поиска среднего аримфетического
        int size = a.length;
        double sum = 0;
        for (int i = 0; i < size;i++){//сумма всех числел
            sum += a[i];
        }
        double avg = 0;
        avg = sum/size;
        return avg;
    }

}
