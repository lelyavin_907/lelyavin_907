import  java.util.*;
public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        String x1 = Integer.toString(x);
        int n = x1.length();
        int[] arr = new int[n];
        int num = in.nextInt();
        int x2 = Integer.parseInt(x1);
        int number = 0;

        for (int i = 0;i < n; i++) {
            arr[i] = x2 % 10;
            x2 /= 10;
            number += arr[i] * num;
            num *= 10;

        }
        System.out.println(number);
    }
}
