import java.util.Scanner;
public class Task4{

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("введите число нечетное");
        int n = scanner.nextInt();
        int [] arr1 = new int[n*n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i >= j) && (i >= n - 1 - j) || (i <= j) && (i <= n - 1 - j)) {
                    arr1[i] = 0;
                    System.out.print(arr1[i]);
                }
                else {
                    arr1[i] = 1;
                    System.out.print(arr1[i]);
                }
            }
            System.out.println();
        }
    }
}