
public class RationalFraction {
    private int numerator;
    private int znam;

    RationalFraction() {
        this(0, 1);
    }

    RationalFraction(int numerator, int znam) {
        if (znam != 0) {
            this.numerator = numerator;
            this.znam = znam;
        } else
            System.out.println("Вы ввели дробь с знаменателем = 0");
    }

    private int NOD(int numerator, int znam) {
        znam = Math.abs(znam);
        numerator = Math.abs(numerator);
        while ((znam > 0) && (numerator > 0)) {
            if (znam > numerator) {
                znam %= numerator;
            } else {
                numerator %= znam;
            }
        }
        return znam + numerator;
    }

    public void Reduce() {
        int nod = NOD(numerator, znam);
        this.numerator /= nod;
        this.znam /= nod;
    }

    public RationalFraction add(RationalFraction secondDrobe) {
        int finalZnam = this.znam * secondDrobe.znam;
        int finalNumerator = this.numerator * secondDrobe.znam + secondDrobe.numerator * this.znam;
        int nod = NOD(finalNumerator, finalZnam);
        return new RationalFraction(finalNumerator / nod, finalZnam / nod);
    }


    public void add2(RationalFraction secondDrobe) {
        this.numerator += secondDrobe.numerator;
        this.znam += secondDrobe.znam;
    }

    public RationalFraction sub(RationalFraction secondDrobe) {
        int finalZnam = this.znam * secondDrobe.znam;
        int finalNumerator = this.numerator * secondDrobe.znam - secondDrobe.numerator * this.znam;
        int nod = NOD(finalNumerator, finalZnam);
        return new RationalFraction(finalNumerator / nod, finalZnam / nod);
    }

    public void sub2(RationalFraction secondDrobe) {
        RationalFraction drobe = sub(secondDrobe);
        this.numerator = drobe.numerator;
        this.znam = drobe.znam;
    }


    public RationalFraction mult(RationalFraction secondDrobe) {
        int finalZnam = this.znam * secondDrobe.znam;
        int finalNumerator = this.numerator * secondDrobe.numerator;
        int nod = NOD(finalNumerator, finalZnam);
        return new RationalFraction(finalNumerator / nod, finalZnam / nod);
    }

    public void mult2(RationalFraction secondDrobe) {
        RationalFraction drobe = mult(secondDrobe);
        this.numerator = drobe.numerator;
        this.znam = drobe.znam;
    }

    public RationalFraction div(RationalFraction secondDrobe) {
        int finalNumerator = this.numerator * secondDrobe.znam;
        int finalZnam = this.znam * secondDrobe.numerator;
        int nod = NOD(finalNumerator, finalZnam);
        return new RationalFraction(finalNumerator / nod, finalZnam / nod);
    }

    public void div2(RationalFraction secondDrobe) {
        RationalFraction drobe = div(secondDrobe);
        this.numerator = drobe.numerator;
        this.znam = drobe.znam;
    }

    public String toString() {
        return "(" + this.numerator + "/" + this.znam + ")";
    }

    public double value() {
        return (double) this.numerator / this.znam;
    }

    public boolean equals(RationalFraction secondDrobe) {
        boolean b = false;
        int thisNod = NOD(this.numerator, this.znam);
        int secondNod = NOD(secondDrobe.numerator, secondDrobe.znam);
        if (((this.numerator / thisNod) == (secondDrobe.numerator / secondNod)) && (this.znam / thisNod) == (secondDrobe.znam / secondNod)) {
            return b = true;
        } else {

            return b;
        }
    }

    public int numberPart() {
        if (this.numerator / this.znam == 0) {
            return 0;
        } else {
            return this.numerator / this.znam;
        }
    }
}
