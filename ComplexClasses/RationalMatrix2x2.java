public class RationalMatrix2x2 {
    private int n = 2;
    private RationalFraction[][] array = new RationalFraction[n][n];
    private RationalVector2D rationalVector2D1 = new RationalVector2D();


    RationalMatrix2x2() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.array[i][j] = new RationalFraction();
            }
        }
    }

    RationalMatrix2x2(RationalFraction rationalFraction1, RationalFraction rationalFraction2, RationalFraction rationalFraction3, RationalFraction rationalFraction4) {
        array[0][0] = rationalFraction1;
        array[0][1] = rationalFraction2;
        array[1][0] = rationalFraction3;
        array[1][1] = rationalFraction4;

    }

    RationalMatrix2x2(RationalFraction[][] array2) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.array[i][j] = array2[i][j];
            }
        }
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 rationalMatrix2x2) {
        RationalFraction[][] arrayLocal = new RationalFraction[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arrayLocal[i][j] = this.array[i][j].add(rationalMatrix2x2.array[i][j]);
            }
        }
        return new RationalMatrix2x2(arrayLocal);
    }

    public RationalMatrix2x2 mult(RationalMatrix2x2 rationalMatrix2x2) {
        RationalFraction[][] arrayLocal = new RationalFraction[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arrayLocal[i][j] = this.array[i][j].mult(rationalMatrix2x2.array[i][j]);
            }
        }
        return new RationalMatrix2x2(arrayLocal);
    }

    public RationalFraction det() {
        this.array[0][0].mult2(this.array[1][1]);
        this.array[1][0].mult2(this.array[0][1]);
        return array[0][0].sub(array[1][0]);
    }

    public RationalVector2D multVector(RationalVector2D rationalVector2D) {
        RationalFraction rationalFractionLocal1 = new RationalFraction();
        RationalFraction rationalFractionLocal2 = new RationalFraction();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((j == 0) && (i == 0)) {
                    rationalFractionLocal1 = this.array[i][j].mult(rationalVector2D.getRationalFraction1());
                } else if ((j == 1) && (i == 0)) {
                    this.array[i][j].mult2(rationalVector2D.getRationalFraction2());
                    rationalFractionLocal1.add2(this.array[i][j]);
                } else if ((j == 0) && (i == 1)) {
                    rationalFractionLocal2 = this.array[i][j].mult(rationalVector2D.getRationalFraction1());

                } else {
                    this.array[i][j].mult2(rationalVector2D.getRationalFraction2());
                    rationalFractionLocal2.add(this.array[i][j]);
                }
            }
        }
        return new RationalVector2D(rationalFractionLocal1, rationalFractionLocal2);
    }


}
