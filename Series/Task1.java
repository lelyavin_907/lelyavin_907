import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите колличество элементов");
        int n = in.nextInt();
        System.out.println("Введите x");
        int x = in.nextInt();
        int realX = 1;
        double lastSum = 0;
        for (double k = 1; k <= n; k++) {
            realX *= x;
            lastSum += k * realX;
        }
        System.out.println(lastSum);
    }
}
