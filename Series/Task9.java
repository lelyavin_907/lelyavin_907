import java.util.*;

public class Task9 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите колличество элементов");
        int n = in.nextInt();
        double sum = 0;
        int powerNumerator = -1;
        double powerZnam = 2;
        for (int k = 1; k <= n; k++) {
            powerNumerator *= powerNumerator;
            sum += (2 + powerNumerator) / powerZnam;
            powerZnam *= powerZnam;
        }
        double roundOff = Math.round(sum * 100.0) / 100.0;
        System.out.println(roundOff);
    }
}
