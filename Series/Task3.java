import java.util.*;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите колличество элементов");
        int n = in.nextInt();
        double firstSum = 1;
        double firstX = 0.8;
        double secondSum = 1;
        double secondX = 0.3;
        for (int k = 0; k < n; k++) {
            firstSum += firstX;
            firstX *= firstX;
            secondSum += secondX;
            secondX *= secondX;
        }
        if (n > 0) {
            double lastSum = firstSum + (-3 * secondSum);
            double roundOff = Math.round(lastSum * 100.0) / 100.0;
            System.out.println(roundOff);
        } else {
            System.out.println("Вы ввели неправильное количество элементов");
        }
    }
}
