import java.util.*;

public class Task5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите колличество элементов");
        int n = in.nextInt();
        double sum = -1;
        double lastSum = 0;
        for (int k = 1; k <= n; k++) {
            for (int i = 0; i < k + 1; i++) {
                sum *= -1;
            }
            lastSum += sum;

        }
        System.out.println(lastSum);
    }
}
