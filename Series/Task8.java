import java.util.*;

public class Task8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите колличество элементов");
        int n = in.nextInt();
        double sum = 1;
        int factorial = 1;
        double powerZnamenatel = 1;
        for (int k = 1; k <= n; k++) {
            factorial *= k;
            powerZnamenatel = 1;
            for (int i = 0; i < k * k; i++) {
                powerZnamenatel *= 2;
            }
            sum += (factorial * factorial) / powerZnamenatel;
        }
        double roundOff = Math.round(sum * 100.0) / 100.0;
        System.out.println(roundOff);
    }
}
