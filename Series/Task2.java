import java.util.*;

public class Task2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите колличество элементов");
        int n = in.nextInt();
        int factorial = 1;
        double numerator = 1;
        double sum = 0;
        int i = 1;
        for (int k = 0; k <= n - 1; k++) {
            sum += numerator / factorial;
            numerator *= 3;
            factorial *= (2 * i) * (2 * i + 1);
            i++;
        }
        double roundOff = Math.round(sum * 100.0) / 100.0;
        System.out.println(roundOff);
    }
}
