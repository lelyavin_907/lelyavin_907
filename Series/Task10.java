import java.util.*;

public class Task10 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите колличество элементов");
        int n = in.nextInt();
        double e = 2.718;
        double sum = 0;
        int factorialK = 1;
        int powerK = 1;
        for (int k = 1; k <= n; k++) {
            factorialK *= k;
            powerK *= k;
            sum += (factorialK * e) / powerK;
            e *= e;
        }
        double roundOff = Math.round(sum * 100.0) / 100.0;
        System.out.println(roundOff);
    }
}
