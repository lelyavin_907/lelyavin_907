import java.util.*;

public class Task4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите колличество элементов");
        int n = in.nextInt();
        double sum = 0;
        for (double k = 1; k <= n; k++) {
            sum += (2 * k + 1) / ((k * k) * (k + 1) * (k + 1));
        }
        double roundOff = Math.round(sum * 100.0) / 100.0;
        System.out.println(roundOff);
    }
}
