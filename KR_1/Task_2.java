import java.util.*;

public class Task_2 {
    public static double pow(double a, int b) {
        double s = 1.0;
        for (int i = 0; i < b; i++) {
            s *= a;
        }
        return s;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите n");
        int n = in.nextInt();
        System.out.println("Введите x");
        int x = in.nextInt();
        int x_factorial = 1;
        double sum = 0;
        double sum2 = 0;
        int factorial = 0;
        int second_fac = 2;
        double last_sum = 0;
        int i_factorial = 1;
        sum = pow(x, 3) / 2;//первое слагаемое (оно статично)
        for (int i = 1; i <= x; i++) {//программа находит факториал для x
            x_factorial *= i;
        }
        for (int i = 2; i <= n; i++) {
            i_factorial = 1;
            for (int j = 1; j <= 2 * i; j++) {//факториал для i
                i_factorial *= j;
            }
            sum2 += ((pow(x, i * 2)) * x_factorial) / i_factorial;//второе слагаемое
        }
        last_sum = sum + sum2;
        System.out.println(last_sum);
    }
}
