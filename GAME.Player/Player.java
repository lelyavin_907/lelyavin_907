
public class Player {
    private String name;
    private int hp = 50;

    public int getHp() {
        return this.hp;
    }
    public int chance(int power){
        double probability = 1.0/power;
        if (probability >= Math.random()){
            System.out.println(probability);
            return power;
        }
        else
        return 0;
    }

    public void damage(int power) {
        int chance = chance(power);
        if ((power <= 9) && (power >= 1)) {
            this.hp -= chance;
        }
    }
}
