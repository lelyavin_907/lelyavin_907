import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Player p1 = new Player();
        Player p2 = new Player();

        int i = 0;
        while ((p1.getHp() > 0) && (p2.getHp() > 0)) {
            if (i % 2 == 0) {
                System.out.println("p1 hits(power damage <=9 and >=1: ");
                int power = in.nextInt();
                p2.damage(power);
                System.out.println(p2.getHp());
            }
            if (i % 2 == 1) {
                System.out.println("p2 hits(power damage <=9 and >=1: ");
                int power = in.nextInt();
                p1.damage(power);
                System.out.println(p1.getHp());
            }
            if (p2.getHp() <= 0) {
                System.out.println("Game over");
            }
            i++;
        }
        System.out.println(p2.getHp());
    }
}
