public class Fraction {
    private int p,q;

    public Fraction(int p, int q ) {
        if (q < 0){
            q *= -1;
            p *= -1;
        }
        this.p = p;
        this.q = q;
        Fraction.normalize(this);
    }

    public Fraction add(Fraction f2) {
        return new Fraction(this.p * f2.q + f2.p * this.q,this.q * f2.q);

    }
    public Fraction mul(Fraction f2) {
        return new Fraction(this.p * f2.p,this.q * f2.q);

    }

    public Fraction sub(Fraction f2){
        return this.add(f2.mul(-1));
    }
    public Fraction mul(int m){
        return new Fraction(this.p * m, this.q);
    }
    public  double toDouble(){
        return  ((double) this.p / this.q);
    }

    public String toString() {
        return Integer.toString(this.p) + "/" + Integer.toString(this.q);
    }

    private static void normalize(Fraction f) {
        int g = Fraction.gcd(f.p, f.q);
        f.p /= g;
        f.q /= g;
    }

    private static int gcd(int a, int b ) {
    a = Math.abs(a);
    b = Math.abs(b);
    while((a > 0) && (b > 0)){
        if (a > b)
            a %= b;
        else
            b %=a;
    }
    return a + b;
    }
}
