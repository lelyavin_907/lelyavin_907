import java.util.Scanner;

public class Task3{      //я давно делал задание могу ошибаться в комментирование
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x2 = in.nextInt();
        int x = in.nextInt();
        boolean b = x > x2;

        while(true) {
            if (x == 0) {    //при х = 0 останавливать программу
                b = false;
                break;
            }
            if (x == x2) {   //последовательность должна строго возрастать
                b = false;
            }
            if (x < x2) {    //если возрастает
                while (x != 0) {
                    if ((x == x2) || (x > x2)) {
                        b = false;
                    }
                    x2 = x;
                    x = in.nextInt();
                }
                break;
            }

            x2 = x;
            x = in.nextInt();
        }

        System.out.println(b ? "Right sequence" : "Wrong sequence");
    }
}
