import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        boolean b = true; 
        for (int a = 2; a <= x / 2; a++) {  //проверка чисел до половины x т.к. число большее половины не может делится на x

            if (x % a == 0) {  //проверка числа на простоту(если число делится на что-то,то остаток равен 0)
                System.out.println("не простое число");
                b = false;

            }

        } 
        if (b)  //раз число не делится не на что,то он простое
            System.out.println("простое");


    }
}