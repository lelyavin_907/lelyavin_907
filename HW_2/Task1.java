import java.util.Scanner;

public class Task1 {

    public static double pow(double a, int b) { //создаю функцию для возведения в степень.
        double s = 1.0;
        for(int i = 0; i < b; i++) {
            s *= a;
        }
        return s;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double x = in.nextDouble();
        int n = in.nextInt();

        double res = 0.0;

        for(int k = 1; k <= n; k++) {
            res += pow(x, 3 * k) / pow(k, 2 * k);//возведение чисел в степень и сложение функций
        }

        System.out.println(res);
    }
}
