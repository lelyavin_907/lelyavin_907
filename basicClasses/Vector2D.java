public class Vector2D {
    public double x;
    public double y;

    Vector2D() {
        this.x = 0.0;
        this.y = 0.0;
    }

    Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D add(Vector2D secondVektor) {
        Vector2D result = new Vector2D(this.x + secondVektor.x, this.y + secondVektor.y);
        return result;
    }

    public void add2(Vector2D secondVector) {
        this.x += secondVector.x;
        this.y += secondVector.y;
    }

    public Vector2D sub(Vector2D secondVector) {
        return new Vector2D(this.x - secondVector.x, this.y - secondVector.y);
    }

    public void sub2(Vector2D secondVector) {
        this.x -= secondVector.x;
        this.y -= secondVector.y;
    }

    public Vector2D mult(Vector2D second) {
        return new Vector2D(this.x * x, this.y * x);
    }

    public void mult2(Vector2D variable) {
        this.x *= x;
        this.y *= x;
    }

    public String toString() {
        return "(" + this.x + " " + this.y + ")";
    }

    public double length() {
        double result = Math.sqrt(this.x * this.x + this.y * this.y);
        return result;
    }

    public double cos(Vector2D secondVector) {
        return (this.x * secondVector.x + this.y * secondVector.y) / (this.length() * secondVector.length());
    }

    public double scalarProduct(Vector2D secondVector) {
        return (this.length() * secondVector.length() * this.cos(secondVector));
    }

    public boolean equals(Vector2D secondVector) {
        if (this.length() == secondVector.length()) {
            return true;
        }
        return false;

    }
}
