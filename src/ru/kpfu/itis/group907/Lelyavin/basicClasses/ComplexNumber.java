package ru.kpfu.itis.group907.Lelyavin.basicClasses;

public class ComplexNumber {
    protected double real;
    private double imaginary;

    public ComplexNumber() {
        this(0, 0);
    }

    public ComplexNumber(double real, double complex) {
        this.real = real;
        this.imaginary = complex;
    }

    public ComplexNumber add(ComplexNumber secondComplex) {
        return new ComplexNumber(this.real + secondComplex.real, this.imaginary + secondComplex.imaginary);
    }

    public void add2(ComplexNumber secondComplex) {
        ComplexNumber complex = add(secondComplex);
        this.real = complex.real;
        this.imaginary = complex.imaginary;
    }

    public ComplexNumber sub(ComplexNumber secondComplex) {
        return new ComplexNumber(this.real - secondComplex.real, this.imaginary - secondComplex.imaginary);
    }

    public void sub2(ComplexNumber secondComplex) {
        ComplexNumber complex = sub(secondComplex);
        this.real = complex.real;
        this.imaginary = complex.imaginary;
    }

    public ComplexNumber mult(ComplexNumber secondComplex) {
        return new ComplexNumber((this.real * secondComplex.real - this.imaginary * secondComplex.imaginary), (this.real * secondComplex.imaginary + secondComplex.real * this.imaginary));
    }

    public void mult2(ComplexNumber secondComplex) {
        this.real = this.real * secondComplex.real - this.imaginary * secondComplex.imaginary;
        this.imaginary = this.real * secondComplex.imaginary + secondComplex.real * this.imaginary;
    }

    public ComplexNumber multNumber(int x) {
        return new ComplexNumber(this.real * x, this.imaginary * x);
    }

    public void multNumber2(int x) {
        this.real *= x;
        this.imaginary *= x;
    }

    public ComplexNumber div(ComplexNumber secondComplex) {
        return new ComplexNumber((this.real * secondComplex.real + this.imaginary * secondComplex.imaginary) / ((secondComplex.real) * (secondComplex.real) + (secondComplex.imaginary) * (secondComplex.imaginary)), (secondComplex.real * this.imaginary - this.real * secondComplex.imaginary) / ((secondComplex.real) * (secondComplex.real) + (secondComplex.imaginary) * (secondComplex.imaginary)));
    }

    public void div2(ComplexNumber secondComplex) {
        this.real = (this.real * secondComplex.real + this.imaginary * secondComplex.imaginary) / ((secondComplex.real) * (secondComplex.real) + (secondComplex.imaginary) * (secondComplex.imaginary));
        this.imaginary = ((secondComplex.real * this.imaginary - this.real * secondComplex.imaginary) / ((secondComplex.real) * (secondComplex.real) + (secondComplex.imaginary) * (secondComplex.imaginary)));
    }

    public double length() {
        double module;
        return module = Math.sqrt(this.real * this.real + this.imaginary * this.imaginary);
    }

    public String toString() {
        if (this.imaginary < 0) {
            return this.real + " " + " " + "- i * " + (-this.imaginary) ;
        } else return this.real + " + " + "i * " + this.imaginary ;
    }

    public double arg() {
        if ((this.real != 0) && (this.imaginary != 0)) {
            double arg = Math.atan((double) this.imaginary / this.real);
            return arg;
        } else return 0;
    }

    public ComplexNumber pow(int pow) {
        return new ComplexNumber( Math.pow(this.length(), pow) * (Math.cos(this.arg() * pow)), Math.pow(this.length(), pow) * Math.sin(this.arg() * pow));
    }

    public boolean equals(ComplexNumber secondComplex) {
        boolean b = false;
        if ((this.real == secondComplex.real) && (this.imaginary == secondComplex.imaginary)) {
            b = true;
            return b;
        } else return b;
    }
}






