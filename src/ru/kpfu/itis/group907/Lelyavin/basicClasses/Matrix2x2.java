package ru.kpfu.itis.group907.Lelyavin.basicClasses;

public class Matrix2x2 {
    private static final int n = 2;
    private double a[][] = new double[n][n];

   public Matrix2x2() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.a[i][j] = 0;
            }
        }
    }

    public Matrix2x2(double element) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.a[i][j] = element;
            }
        }
    }

    public Matrix2x2(double[][] a) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.a[i][j] = a[i][j];
            }
        }
    }

    public Matrix2x2(double element1, double element2, double element3, double element4) {
        a[0][0] = element1;
        a[0][1] = element2;
        a[1][0] = element3;
        a[1][1] = element4;
    }

    public Matrix2x2 add(Matrix2x2 b) {
        double[][] c = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                c[i][j] = this.a[i][j] + b.a[i][j];
            }
        }
        return new Matrix2x2(c);
    }

    public void add2(Matrix2x2 b) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.a[i][j] += b.a[i][j];
            }
        }
    }

    public Matrix2x2 multNumber(int x) {
        double[][] c = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                c[i][j] = x * this.a[i][j];
            }
        }
        return new Matrix2x2(c);
    }

    public Matrix2x2 sub(Matrix2x2 b) {
        return this.add(b.multNumber(-1));
    }

    public void sub2(Matrix2x2 b) {
        this.add2(b.multNumber(-1));
    }

    public void multNumber2(int x) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.a[i][j] *= x;
            }
        }
    }

    public Matrix2x2 mult(Matrix2x2 b) {
        double[][] c = new double[n][n];
        for (int i = 0; i < n; i++) {
            //System.out.println();
            for (int j = 0; j < n; j++) {
                c[i][j] = this.a[i][0] * b.a[0][j] + this.a[i][1] * b.a[1][j];
            }
        }
        return new Matrix2x2(c);
    }

    public void mult2(Matrix2x2 b) {
        double[][] c = new double[n][n];
        for (int i = 0; i < n; i++) {
            //System.out.println();
            for (int j = 0; j < n; j++) {
                c[i][j] = this.a[i][0] * b.a[0][j] + this.a[i][1] * b.a[1][j];
            }
        }
        this.a = c;
    }

    public double det() {
        double det = this.a[0][0] * this.a[1][1] - this.a[1][0] * this.a[0][1];
        return det;
    }

    public Matrix2x2 transpon() {
        double saveElement;
        for (int i = 0; i < n / 2; i++) {
            //System.out.println();
            for (int j = 0; j < n; j++) {
                saveElement = this.a[i][j];
                this.a[i][j] = this.a[j][i];
                this.a[j][i] = saveElement;
            }
        }
        return new Matrix2x2(this.a);
    }

    public Matrix2x2 inverseMatrix() {
        double[][] c = new double[n][n];
        double localDet = det();
        double saveElement;
        if (localDet > 0) {
            for (int i = 0; i < n / 2; i++) {
                //System.out.println();
                for (int j = 0; j < n; j++) {
                    saveElement = this.a[i][j];
                    this.a[i][j] = this.a[j][i];
                    this.a[j][i] = saveElement;
                }
            }
            c = this.a;
            return new Matrix2x2(c);
        } else {
            for (int i = 0; i < n / 2; i++) {
                //System.out.println();
                for (int j = 0; j < n; j++) {
                    this.a[i][j] = 0;
                }
            }
            c = this.a;
            return new Matrix2x2(c);
        }
    }

    public Matrix2x2 equivalentDiagonal() {
        double saveElement = a[0][0];
        double saveElement2 = a[1][0];
        double[][] b = new double[n][n];
        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n; j++) {
                b[i][j] = this.a[i][j];
            }
        }
        b[0][0] *= saveElement2;//до saveElement3 происходит преобразование a[1][0] в 0.
        b[0][1] *= saveElement2;
        b[1][0] = b[1][0] * saveElement - b[0][0];
        b[1][1] = b[1][1] * saveElement - b[0][1];
        double saveElement3 = b[1][1];// происходит преобразование b[0][1] в 0.
        b[1][1] *= b[0][1];
        b[0][1] = b[0][1] * saveElement3 - b[1][1];
        b[0][0] *= saveElement3;

        return new Matrix2x2(b);
    }

    public String toString() {
        return "\n" + this.a[0][0] + " " + this.a[0][1] + "\n" + this.a[1][0] + " " + this.a[1][1];
    }

    public Vector2D multVector(Vector2D vector2D) {
        Vector2D c = new Vector2D();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((j == 0) && (i == 0)) {
                    c.x = this.a[i][j] * vector2D.x;
                } else if ((j == 1) && (i == 0)) {
                    c.y = this.a[i][j] * vector2D.y;
                } else if ((j == 0) && (i == 1)) {
                    c.x += this.a[i][j] * vector2D.x;
                } else {
                    c.y += this.a[i][j] * vector2D.y;
                }
            }
        }
        return new Vector2D(c.x, c.y);
    }
}
