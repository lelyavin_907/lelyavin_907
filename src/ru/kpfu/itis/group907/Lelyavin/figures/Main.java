package ru.kpfu.itis.group907.Lelyavin.figures;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(2, 5);
        Circle circle = new Circle(5);
        Triangle triangle = new Triangle(5,5,2);
        Figure[] figures = new Figure[3];
        figures[0] = rectangle;
        figures[1] = circle;
        figures[2] = triangle;
        for (int i = 0; i <= 2; i++) {
            System.out.println(figures[i]);
        }
    }
}
