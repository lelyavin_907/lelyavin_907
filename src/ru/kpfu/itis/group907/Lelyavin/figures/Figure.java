package ru.kpfu.itis.group907.Lelyavin.figures;

public abstract class Figure {

    public String toString() {
        return area() + " " + perimeter();
    }

    public abstract double area();

    public abstract double perimeter();

}
