package ru.kpfu.itis.group907.Lelyavin.figures;

public class Triangle extends Figure {
    private double side1;
    private double side2;
    private double side3;

    public Triangle(double side1, double side2, double side3){
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public double area(){
        return Math.sqrt((this.perimeter() / 2) * (perimeter() / 2 - side1) * (perimeter() / 2 - side2) * (perimeter() / 2 - side3));
    }

    public double perimeter(){
        return (side1 + side2 + side3);
    }


    public String print() {
        return "Triangle";
    }

    public String toString() {
        return this.print() + " " + this.area() + " " + this.perimeter();
    }
}
