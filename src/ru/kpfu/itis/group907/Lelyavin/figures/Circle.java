package ru.kpfu.itis.group907.Lelyavin.figures;

public class Circle extends Figure {
    private double radius;

    public Circle(double radius){
        this.radius = radius;
    }
    public double area() {
        return (Math.PI * radius * radius);
    }

    public double perimeter() {
        return (2 * Math.PI * radius);
    }

    public String print() {
        return "Circle";
    }

    public String toString() {
        return this.print() + " " + this.area() + " " + this.perimeter();
    }
}
