package ru.kpfu.itis.group907.Lelyavin.figures;

public class Rectangle extends Figure implements printable {
    private double side1;
    private double side2;


    public Rectangle(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public double area() {
        return (this.side1 * this.side2);
    }

    public double perimeter() {
        return (this.side1 + this.side2) * 2;
    }

    public String print() {
        return "Rectangle";
    }

    public String toString() {
        return this.print() + " " + this.area() + " " + this.perimeter();
    }
}
