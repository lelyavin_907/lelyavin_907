package ru.kpfu.itis.group907.Lelyavin.kontrol;

public class Salad {
    private String[] ingredient;

    Salad(){
            this.ingredient[0] = "";
    }
    public String[] getIngredient(){
        return this.ingredient;
    }

    public String[] prepare(String[] strings) {

        for (int i = 0; i < strings.length; i++) {
            switch (strings[i]) {
                case "Onion":
                    this.ingredient[i] = strings[i];
                    break;
                case "Ham":
                    this.ingredient[i] = strings[i];
                    break;
                case "Egg":
                    this.ingredient[i] = strings[i];
                    break;
                case "Tomato":
                    this.ingredient[i] = strings[i];
                    break;
                case "Salad":
                    this.ingredient[i] = strings[i];
                    break;
                case "Potato":
                    this.ingredient[i] = strings[i];
                    break;
            }
        }
        return this.ingredient;
    }
    public String cook(){
        return "Salad" + this.ingredient;
    }

    public String[] print(){
        return this.ingredient;
    }
}
