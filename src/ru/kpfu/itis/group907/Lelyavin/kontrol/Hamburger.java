package ru.kpfu.itis.group907.Lelyavin.kontrol;

public class Hamburger {
    private String[] ingredient;

    public String[] getIngredient(){
        return this.ingredient;
    }

    Hamburger(){
            int countIngredient = 4;
            this.ingredient = new String[countIngredient];
            //тост,масло,желе,тост
            this.ingredient[0] = "";
            this.ingredient[1] = "";
            this.ingredient[2] = "";
            this.ingredient[3] = "";
    }
    public String[] prepare(String nameBurger){
        if (nameBurger.equals("jelly-butter sandwich")){
            int countIngredient = 4;
            this.ingredient = new String[countIngredient];
            //тост,масло,желе,тост
            this.ingredient[0] = "Bread";
            this.ingredient[1] = "Butter";
            this.ingredient[2] = "Jelly";
            this.ingredient[3] = "Bread";
            return this.ingredient;
        }
        else if (nameBurger.equals("ham")){
            int countIngredient = 3;
            this.ingredient = new String[countIngredient];
            this.ingredient[0] = "Bread";
            this.ingredient[1] = "Ham";
            this.ingredient[2] = "Bread";
        }
        return this.ingredient;
    }

    public String[] print(){
        return this.ingredient;
    }

}
