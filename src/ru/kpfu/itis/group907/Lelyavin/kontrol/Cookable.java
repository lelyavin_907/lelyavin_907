package ru.kpfu.itis.group907.Lelyavin.kontrol;

public interface Cookable {

    public String[] prepare();

    public String cook();
}
