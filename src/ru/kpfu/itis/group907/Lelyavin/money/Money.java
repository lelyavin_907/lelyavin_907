package ru.kpfu.itis.group907.Lelyavin.money;

public class Money {
    private int rubs;
    private int kopeck;

    public Money() {
        rubs = 0;
        kopeck = 0;
    }

    public String toString() {
        return this.rubs + "," + this.kopeck;
    }

    public Money(int rubs, int kopeck) {
        this.rubs = rubs;
        this.kopeck = kopeck;
    }

    public Money add(Money secondMoney) {
        this.rubs += secondMoney.rubs;
        this.kopeck += secondMoney.kopeck;
        return (this.parsing());
    }

    public Money sub(Money secondMoney) {
        this.rubs -= secondMoney.rubs;
        this.kopeck -= secondMoney.kopeck;
        return (this.parsing());
    }

    public double div(Money secondMoney) {
        double thisKopeck = (double) this.kopeck / 100;
        double secondKopeck = (double) secondMoney.kopeck / 100;
        double thisRubs = this.rubs + thisKopeck;
        double secondRubs = secondMoney.rubs + secondKopeck;
        return (thisRubs / secondRubs);
    }

    public Money divNumber(double number) {
        this.rubs /= number;
        this.kopeck /= number;
        return (this.parsing());
    }

    public Money multNumber(double number) {
        this.rubs *= number;
        this.kopeck *= number;
        return (this.parsing());
    }

    public boolean equals(Money secondMoney) {
        boolean b = false;
        if ((this.rubs == secondMoney.rubs) && (this.kopeck == secondMoney.kopeck)) {
            b = true;
            return b;
        } else return b;
    }

    public Money parsing() {
        while (kopeck > 99) {
            rubs++;
            kopeck -= 100;
        }
        while (kopeck < -99) {
            rubs--;
            kopeck += 100;
        }
        if ((kopeck > -99) && (kopeck < 0)) {
            kopeck = Math.abs(kopeck);
        }
        return this;
    }
}
