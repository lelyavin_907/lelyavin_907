package ru.kpfu.itis.group907.Lelyavin.halloween;

public class Pumpkin {
    private int halloweenTown;
    private Skeleton skeleton;

    public Pumpkin(int halloweenTown,Skeleton skeleton){
        this.halloweenTown = halloweenTown;
        this.skeleton = skeleton;
    }

    public String toString(){
        return this.skeleton + " " + this.halloweenTown;
    }
}
