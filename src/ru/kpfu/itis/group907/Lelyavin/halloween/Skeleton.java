package ru.kpfu.itis.group907.Lelyavin.halloween;

public class Skeleton {
    private String bones;
    public String getBones(){
        return bones;
    }
    public void setBones(String bones){
        this.bones = bones;
    }

    public Skeleton(String bones){
        this.bones = bones;
    }

    public String toString(){
        return this.bones;
    }
}
