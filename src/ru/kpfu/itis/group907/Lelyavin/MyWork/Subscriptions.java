package ru.kpfu.itis.group907.Lelyavin.MyWork;

public class Subscriptions {
    private int subscriber_id;
    private int subscription_id;


    public Subscriptions(Users subscriber, Users subscription){
        this.subscriber_id = subscriber.getId();
        this.subscription_id = subscription.getId();
    }

    public String toString(){
        return this.subscriber_id + " subscribe to " + this.subscription_id;
    }
}
