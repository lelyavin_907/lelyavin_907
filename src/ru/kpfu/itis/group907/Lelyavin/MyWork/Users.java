package ru.kpfu.itis.group907.Lelyavin.MyWork;

public class Users {
    private int id;
    private String username;
    private String password;
    private String gender;
    private String email;
    public int getId(){
        return id;
    }

    public String getUsername(){
        return this.username;
    }

    public Users(){
        this.id = 0;
    }

    public void setId(int id){
        this.id = id;
    }

    public Users(String string) {
        String[] flightsInfo = string.split(" ");
        this.id = Integer.parseInt(flightsInfo[0]);
        this.username = flightsInfo[1];
        this.password = flightsInfo[2];
        this.gender = flightsInfo[3];
        this.email = flightsInfo[4];
    }

    public String toString(){
        return  "User{ it's you id: " + this.id + ", is's you username: " + this.username + ", it's you password: " + this.password + ", it's you gender: " + this.gender + ", it's you email:  " + this.email + " }";
    }

}
