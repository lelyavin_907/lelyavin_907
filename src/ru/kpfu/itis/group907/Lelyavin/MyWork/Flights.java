package ru.kpfu.itis.group907.Lelyavin.MyWork;

public class Flights {
    private int airlineNo;
    private int flightsNo;
    private String countryFrom;
    private String countryTo;
    static final private int x = 12;//количество авиакомпаний
    static final private int n = 1200;
    private static int[] arrayCountAirline = new int[n];

    public int getAirlineNo() {
        return airlineNo;
    }

    public int getFlightsNo() {
        return flightsNo;
    }

    public String getCountryFrom() {
        return countryFrom;
    }

    public Flights() {
        for (int i = 0; i < n; i++) {
            Flights.arrayCountAirline[i] = 0;
        }
    }

    public Flights(String csvString) {
        String[] flightsInfo = csvString.split(",");
        this.airlineNo = Integer.parseInt(flightsInfo[0]);
        this.flightsNo = Integer.parseInt(flightsInfo[1]);
        this.countryFrom = flightsInfo[2];
        this.countryTo = flightsInfo[3];
    }

    @Override
    public String toString() {
        return "Flights{" +
                "airlineNo=" + airlineNo +
                ", flightsNo=" + flightsNo +
                ", countryFrom='" + countryFrom + '\'' +
                ", countryTo='" + countryTo + '\'' +
                '}';
    }
}