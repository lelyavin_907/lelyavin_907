package ru.kpfu.itis.group907.Lelyavin.MyWork;

public class Messages {
    private int sender_id;
    private int receiver_id;
    private String timeSent;
    private String text;
    private enum StatusEnum{READ, UNREAD}
    private StatusEnum status;

    public String getText(){
        return this.text;
    }

    public Messages(String string, Users sender, Users receiver){
        String[] flightsInfo = string.split(",");
        this.sender_id = sender.getId();
        this.receiver_id = receiver.getId();
        this.timeSent = flightsInfo[0];
        this.text = flightsInfo[1];
        status = StatusEnum.valueOf(flightsInfo[2]);
    }

    public String toString(){
        return "{ " + this.sender_id + "  " + this.receiver_id + "  " + this.timeSent + " " + this.text + " " + status + " }";
    }


}
