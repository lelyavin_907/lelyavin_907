package ru.kpfu.itis.group907.Lelyavin.MyWork;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        int countUsers = 2;
        String string1 = "0 Maxim 125 male moxim228";
        String string2 = "1 Nikita 124 male nlelyavin";
        Users[] user = new Users[countUsers];
        Users user0 = new Users(string1);
        Users user1 = new Users(string2);
        user[0] = user0;
        user[1] = user1;

        PrintWriter out = new PrintWriter(new FileWriter("out.csv"));
        for (int j = 0; j < countUsers; j++) {
            out.println(user[j]);
        }
        out.close();

        String stringMassages1 = "17:42 , hello  bro ,READ";
        String stringMassages2 = "17:42 , hello  bro ,READ";
        int countMassages = 2;
        Messages messages1 = new Messages(stringMassages1, user0 , user1);
        Messages messages2 = new Messages(stringMassages2, user1 , user0);
        Messages[] arrayMessages = new Messages[countMassages];
        arrayMessages[0] = messages1;
        arrayMessages[1] = messages2;
        PrintWriter outMassages = new PrintWriter(new FileWriter("outMassages.csv"));
        for (int j = 0; j < countMassages; j++) {
            outMassages.println(arrayMessages[j]);
        }
        outMassages.close();

        int countSubscription = 2;
        Subscriptions subscription1 = new Subscriptions(user0,user1);
        Subscriptions subscription2 = new Subscriptions(user1,user0);
        Subscriptions[] arraySubscriptions = new Subscriptions[countSubscription];
        arraySubscriptions[0] = subscription1;
        arraySubscriptions[1] = subscription2;
        PrintWriter outSub = new PrintWriter(new FileWriter("outSub.csv"));
        for (int j = 0; j < countSubscription; j++) {
            outSub.println(arraySubscriptions[j]);
        }
        outSub.close();

        String name0 = user0.getUsername();
        String name1 = user1.getUsername();
        System.out.println(messages1.getText());
    }
}
