package ru.kpfu.itis.group907.Lelyavin;
import java.util.Scanner;
import ru.kpfu.itis.group907.Lelyavin.basicClasses.*;
import ru.kpfu.itis.group907.Lelyavin.complexClasses.*;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        //Проверка RationalFraction
        RationalFraction ratFrac = new RationalFraction(3, 5);
        RationalFraction ratFrac2 = new RationalFraction(2, 1);
        RationalFraction ratFrac3 = new RationalFraction(3, 2);
        RationalFraction ratFrac4 = new RationalFraction(1321, -314);

        System.out.printf("                           Сокращение: ");
        ratFrac.Reduce();    //Сокращение
        System.out.println(ratFrac);
        System.out.printf("              Строковое представление: ");
        System.out.println(ratFrac.toString());//Строковое представление
        System.out.printf("                      Сложение дробей: ");
        System.out.println((ratFrac.add(ratFrac2)).toString());//Сложение дробей
        System.out.printf(" Сложение дробей с сохранением в this: ");
        ratFrac.add2(ratFrac2);
        System.out.println(ratFrac.toString());//Сложение дробей с сохранением в this
        System.out.printf("                       Деление дробей: ");
        System.out.println((ratFrac3.sub(ratFrac4)).toString());//Деление дробей
        System.out.printf("  Деление дробей с сохранением в this: ");
        ratFrac3.sub2(ratFrac4);
        System.out.println(ratFrac3.toString());//Деление дробей с сохранением в this
        System.out.printf("                     умножение дробей: ");
        System.out.println((ratFrac2.mult(ratFrac4)).toString());//умножение дробей
        System.out.printf("умножение дробей с сохранением в this: ");
        ratFrac2.mult2(ratFrac4);
        System.out.println(ratFrac2.toString());//умножение дробей с сохранением в this
        System.out.printf("                           div дробей: ");
        System.out.println(ratFrac2.div(ratFrac4));//div дробей
        System.out.printf("      div дробей с сохранением в this: ");
        ratFrac2.div2(ratFrac4);
        System.out.println(ratFrac2.toString());//div дробей с сохранением в this
        System.out.printf("             Десятичное представление: ");
        System.out.println(ratFrac.value());//Десятичное представление
        System.out.printf("                    Сравнение дробей - ");
        System.out.println(ratFrac2.equals(ratFrac));//Сравнение дробей
        System.out.printf("                          Целая часть: ");
        System.out.println(ratFrac2.numberPart());//Целая часть

        //Проверка ComplexNumber
        ComplexNumber comNum1 = new ComplexNumber(2, 5);
        ComplexNumber comNum2 = new ComplexNumber(50, -5);
        ComplexNumber comNum3 = new ComplexNumber(2, 7);
        ComplexNumber comNum4 = new ComplexNumber(25, 1);
        System.out.println();
        System.out.printf("                                                       Комплексные числа");
        System.out.println();
        System.out.println();
        System.out.printf("                                                      Сумма: ");
        System.out.println(comNum1.add(comNum2));
        System.out.printf("                                 Сумма с сохранением в this: ");
        comNum1.add2(comNum2);
        System.out.println(comNum1);
        System.out.printf("                            Вычитание из комплексного числа: ");
        System.out.println(comNum2.sub(comNum3));
        System.out.printf("       Вычитание из комплексного числа с сохранением в this: ");
        comNum2.sub2(comNum3);
        System.out.println(comNum2);
        System.out.printf("                     Умножение комплексного на вещественное: ");
        System.out.println(comNum3.multNumber(2));
        System.out.printf("Умножение комплексного на вещественное с сохранением в this: ");
        comNum3.multNumber2(4);//
        System.out.println(comNum3);
        System.out.printf("                      Умножение комплексного на комплексное: ");
        System.out.println(comNum1.mult(comNum4));
        System.out.printf(" Умножение комплексного на комплексное с сохранением в this: ");
        comNum1.mult2(comNum4);//
        System.out.println(comNum1);
        System.out.printf("                        Деление комплексного на комплексное: ");
        System.out.println(comNum1.div(comNum4));
        System.out.printf("   Деление комплексного на комплексное с сохранением в this: ");
        comNum1.div2(comNum4);
        System.out.println(comNum1);
        System.out.printf("                                       Модуль комплексного : ");
        System.out.println(comNum1.length());
        System.out.printf("                                    Строковое представление: ");
        System.out.println(comNum1);
        System.out.printf("                                Аргумент комлпексного числа: ");
        System.out.println(comNum3.arg());
        System.out.printf("                                       Возведение в степень: ");
        System.out.println(comNum1.pow(3));


        System.out.println("\nМатрицы 2х2\n");
        Matrix2x2 a = new Matrix2x2(1, 2, 3, 4);
        Matrix2x2 b = new Matrix2x2(5.5, 6.5, 7.5, 8.5);
        System.out.printf("Сумма: ");
        System.out.println(a.add(b));
        System.out.printf("Сумма с сохранением в this: ");
        a.add2(b);
        System.out.println(a);
        a.sub2(b);
        System.out.printf("Вычитание: ");
        System.out.println(a.sub(b));
        System.out.printf("Вычитание с сохранением в this: ");
        a.sub2(b);
        System.out.println(a);
        a.add2(b);
        System.out.printf("Умножение матрицы на число: ");
        System.out.println(a.multNumber(2));
        System.out.printf("Умножение матрицы на матрицу: ");
        System.out.println(a.mult(b));
        System.out.printf("Умножение матрицы на матрицу с сохранением в this: ");
        a.mult2(b);
        System.out.println(a);
        System.out.printf("Определитель матрицы: ");
        System.out.println(a.det());
        System.out.printf("Транспонированная матрица: ");
        System.out.println(a.transpon());
        System.out.printf("Обраная матрица: ");
        System.out.println(a.inverseMatrix());
        System.out.printf("Эквивалентная диагональная матрица: ");
        System.out.println(a.equivalentDiagonal());
        System.out.printf("Умножение матрцы на вектор: ");
        System.out.println(a.multVector(new Vector2D(-5, 4)));


        System.out.println("\n            Vector2D\n");
        Vector2D vector1 = new Vector2D(2, 3);
        Vector2D vector2 = new Vector2D(5, 2);
        Vector2D vector3 = new Vector2D(1, 10);
        System.out.printf(" Размер вектора:");
        System.out.println(vector1.length());
        System.out.printf("            cos:");
        System.out.println(vector1.cos(vector2));
        System.out.printf("  scalarProduct:");
        System.out.println(vector1.scalarProduct(vector3));
        System.out.printf("         equals:");
        System.out.println(vector1.equals(vector3));


        System.out.println("\n            RationalVector2D");
        RationalFraction ratFrac5 = new RationalFraction(1, 5);
        RationalFraction ratFrac6 = new RationalFraction(5, 20);
        RationalFraction ratFrac7 = new RationalFraction(1, 5);
        RationalFraction ratFrac8 = new RationalFraction(5, 20);
        RationalVector2D rV2D1 = new RationalVector2D(ratFrac5, ratFrac6);
        RationalVector2D rV2D2 = new RationalVector2D(ratFrac7, ratFrac8);
        System.out.println("Сумма RationalVector2D: " + rV2D1.add(rV2D2));
        System.out.println("        Размер вектора: " + rV2D1.length());
        System.out.println("         ScalarProduct: " + rV2D1.scalarProduct(rV2D2));
        System.out.println("               equals - " + rV2D1.equals(rV2D2));


        System.out.println("\n            ComplexVector2D");
        ComplexNumber complexNumber1 = new ComplexNumber(4, 5);
        ComplexNumber complexNumber2 = new ComplexNumber(2, 4);
        ComplexNumber complexNumber3 = new ComplexNumber(2, 10);
        ComplexNumber complexNumber4 = new ComplexNumber(-2, 3);
        ComplexVector2D complexVector2D1 = new ComplexVector2D(complexNumber1, complexNumber2);
        ComplexVector2D complexVector2D2 = new ComplexVector2D(complexNumber3, complexNumber4);
        System.out.println("Сложение ComplexVector2D: " + complexVector2D1.add(complexVector2D2));
        System.out.println("ScalarProductComplexVector2D: " + complexVector2D1.scalarProduct(complexVector2D2));


        System.out.println("\n            RationalMatrix2x2");
        RationalFraction rF1 = new RationalFraction(1, 5);
        RationalFraction rF2 = new RationalFraction(1, 4);
        RationalFraction rF3 = new RationalFraction(1, 2);
        RationalFraction rF4 = new RationalFraction(1, -3);
        RationalMatrix2x2 rationalMatrix2x21 = new RationalMatrix2x2(rF1, rF2, rF2, rF1);
        RationalMatrix2x2 rationalMatrix2x22 = new RationalMatrix2x2(rF3, rF4, rF4, rF3);
        System.out.println("Сложение RationalMatrix2x2: " + rationalMatrix2x21.add(rationalMatrix2x22));
        System.out.println("Сложение RationalMatrix2x2: " + rationalMatrix2x21.mult(rationalMatrix2x22));
        System.out.println("Сложение RationalMatrix2x2: " + rationalMatrix2x21.det());
        System.out.println("Сложение RationalMatrix2x2: " + rationalMatrix2x21.multVector(rV2D1));


        System.out.println("\n            RationalComplexNumber");
        RationalComplexNumber rationalComplexNumber1 = new RationalComplexNumber(rF1, rF3);
        RationalComplexNumber rationalComplexNumber2 = new RationalComplexNumber(rF2, rF4);
        RationalComplexNumber rationalComplexNumber3 = new RationalComplexNumber(rF4, rF2);
        RationalComplexNumber rationalComplexNumber4 = new RationalComplexNumber(rF4, rF4);
        System.out.println("Сложение RationalComplexNumber: " + rationalComplexNumber1.add(rationalComplexNumber2));
        System.out.println("Умножение RationalComplexNumber: " + rationalComplexNumber1.mult(rationalComplexNumber2));
        System.out.println("Вычитание RationalComplexNumber: " + rationalComplexNumber1.sub(rationalComplexNumber2));


        System.out.println("\n            RationalComplexMatrix2x2");
        RationalComplexMatrix2x2 rationalComplexMatrix2x21 = new RationalComplexMatrix2x2(rationalComplexNumber1, rationalComplexNumber2, rationalComplexNumber1, rationalComplexNumber2);
        RationalComplexMatrix2x2 rationalComplexMatrix2x22 = new RationalComplexMatrix2x2(rationalComplexNumber2, rationalComplexNumber3, rationalComplexNumber4, rationalComplexNumber1);
        System.out.println("Сложение rationalComplexMatrix2x2: " + rationalComplexNumber1.add(rationalComplexNumber2));
        System.out.println("Умножение rationalComplexMatrix2x2: " + rationalComplexNumber1.mult(rationalComplexNumber2));
        System.out.println("Det rationalComplexMatrix2x2: " + rationalComplexMatrix2x21.det());


        System.out.println("\n            RationalComplexVector2D");
        RationalComplexVector2D rationalComplexVector2D1 = new RationalComplexVector2D(rationalComplexNumber1, rationalComplexNumber2);
        RationalComplexVector2D rationalComplexVector2D2 = new RationalComplexVector2D(rationalComplexNumber3, rationalComplexNumber4);
        System.out.println("Сложение  RationalComplexVector2D: " + rationalComplexVector2D1.add(rationalComplexVector2D2));
        System.out.println("Умножение  RationalComplexVector2D: " + rationalComplexVector2D1.scalarProduct(rationalComplexVector2D2));
        System.out.println("RationalComplexVector2D: " + rationalComplexVector2D1);
    }
}
