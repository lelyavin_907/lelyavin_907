package ru.kpfu.itis.group907.Lelyavin.complexClasses;
import ru.kpfu.itis.group907.Lelyavin.basicClasses.*;

public class ComplexVector2D {
    private ComplexNumber complexNumber1 = new ComplexNumber();
    private ComplexNumber complexNumber2 = new ComplexNumber();

    public ComplexNumber getComplexNumber1() {
        return complexNumber1;
    }

    public ComplexNumber getComplexNumber2() {
        return complexNumber2;
    }

    ComplexVector2D() {
        this.complexNumber1 = new ComplexNumber();
        this.complexNumber2 = new ComplexNumber();

    }

    public ComplexVector2D(ComplexNumber complexNumber1, ComplexNumber complexNumber2) {
        this.complexNumber1 = complexNumber1;
        this.complexNumber2 = complexNumber2;
    }

    public ComplexVector2D add(ComplexVector2D secondComplexVector2D) {
        return new ComplexVector2D(this.complexNumber1.add(secondComplexVector2D.complexNumber1), this.complexNumber2.add(secondComplexVector2D.complexNumber2));
    }

    public String toString() {
        return "(" + this.complexNumber1 + " , " + this.complexNumber2 + ")";
    }

    public ComplexNumber scalarProduct(ComplexVector2D secondComplexVector2D) {
        this.complexNumber1.mult2(secondComplexVector2D.complexNumber1);
        this.complexNumber2.mult2(secondComplexVector2D.complexNumber2);
        return complexNumber1.add(complexNumber2);
    }
}
