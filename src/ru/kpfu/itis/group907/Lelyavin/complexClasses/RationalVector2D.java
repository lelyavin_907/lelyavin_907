package ru.kpfu.itis.group907.Lelyavin.complexClasses;
import ru.kpfu.itis.group907.Lelyavin.basicClasses.*;

public class RationalVector2D {
    private RationalFraction rationalFraction1 = new RationalFraction();
    private RationalFraction rationalFraction2 = new RationalFraction();
    private Vector2D vector2D1 = new Vector2D();

    public RationalFraction getRationalFraction1() {
        return rationalFraction1;
    }

    public RationalFraction getRationalFraction2() {
        return rationalFraction2;
    }

    public RationalVector2D() {
        this.rationalFraction1 = new RationalFraction();
        this.rationalFraction2 = new RationalFraction();
    }

    public RationalVector2D(RationalFraction rationalFraction, RationalFraction rationalFraction2) {
        this.rationalFraction1 = rationalFraction;
        this.rationalFraction2 = rationalFraction2;
    }

    public RationalVector2D add(RationalVector2D rV2D) {
        return new RationalVector2D(this.rationalFraction1.add(rV2D.rationalFraction1), this.rationalFraction2.add(rV2D.rationalFraction2));
    }

    public String toString() {
        return "(" + this.rationalFraction1 + " , " + this.rationalFraction2 + ")";
    }

    public double length() {
        double b = rationalFraction2.value();
        double a = rationalFraction1.value();
        return Math.sqrt(a * a + b * b);
    }

    public RationalFraction scalarProduct(RationalVector2D rV2D) {
        this.rationalFraction1.mult2(rV2D.rationalFraction1);
        this.rationalFraction2.mult2(rV2D.rationalFraction2);
        return rationalFraction2.add(rationalFraction1);
    }

    public boolean equals(RationalVector2D rV2D) {
        boolean b = false;
        boolean a = false;
        boolean localBoolean = false;
        b = this.rationalFraction1.equals(rV2D.rationalFraction1);
        a = this.rationalFraction2.equals(rV2D.rationalFraction2);
        if ((a) && (b)) {
            localBoolean = true;
            return localBoolean;
        } else return localBoolean;
    }


}

























