package ru.kpfu.itis.group907.Lelyavin.complexClasses;
import ru.kpfu.itis.group907.Lelyavin.basicClasses.*;


public class RationalComplexNumber {
    private RationalFraction rationalFraction1 = new RationalFraction();
    private RationalFraction rationalFraction2 = new RationalFraction();

    public RationalFraction getRationalFraction1() {
        return rationalFraction1;
    }

    public RationalFraction getRationalFraction2() {
        return rationalFraction2;
    }

    public RationalComplexNumber() {
        this.rationalFraction1 = new RationalFraction();
        this.rationalFraction2 = new RationalFraction();
    }

    public RationalComplexNumber(RationalFraction secondRationalFraction1, RationalFraction secondRationalFraction2) {
        this.rationalFraction1 = secondRationalFraction1;
        this.rationalFraction2 = secondRationalFraction2;
    }

    public RationalComplexNumber add(RationalComplexNumber secondComplexNumber) {
        return new RationalComplexNumber(this.rationalFraction2.add(secondComplexNumber.rationalFraction2), this.rationalFraction1.add(secondComplexNumber.rationalFraction2));
    }

    public RationalComplexNumber sub(RationalComplexNumber secondComplexNumber) {
        return new RationalComplexNumber(this.rationalFraction2.sub(secondComplexNumber.rationalFraction2), this.rationalFraction1.sub(secondComplexNumber.rationalFraction2));
    }

    public RationalComplexNumber mult(RationalComplexNumber secondComplexNumber) {
        return new RationalComplexNumber(this.rationalFraction2.mult(secondComplexNumber.rationalFraction2), this.rationalFraction1.mult(secondComplexNumber.rationalFraction2));
    }

    public String toString() {
//        if ((this.rationalFraction2.getNumerator() > 0) && (this.rationalFraction2.getZnam() > 0) || ((this.rationalFraction2.getNumerator() < 0) && (this.rationalFraction2.getZnam() < 0))) {
        return "(" + this.rationalFraction1 + " + " + this.rationalFraction2 + " * i )";

    }
}
