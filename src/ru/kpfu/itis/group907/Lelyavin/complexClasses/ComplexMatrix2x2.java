package ru.kpfu.itis.group907.Lelyavin.complexClasses;
import ru.kpfu.itis.group907.Lelyavin.basicClasses.*;

public class ComplexMatrix2x2 {
    private int n = 2;
    private ComplexNumber[][] array = new ComplexNumber[n][n];

    public ComplexMatrix2x2() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.array[i][j] = new ComplexNumber();
            }
        }
    }

    public ComplexMatrix2x2(ComplexNumber complexNumber1, ComplexNumber complexNumber2, ComplexNumber complexNumber3, ComplexNumber complexNumber4) {
        this.array[0][0] = complexNumber1;
        this.array[0][1] = complexNumber2;
        this.array[1][0] = complexNumber3;
        this.array[1][1] = complexNumber4;
    }

    public ComplexMatrix2x2 add(ComplexMatrix2x2 secondComplexMatrix2x2) {
        ComplexMatrix2x2 complexMatrix2x2Local = new ComplexMatrix2x2();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                complexMatrix2x2Local.array[i][j] = this.array[i][j].add(secondComplexMatrix2x2.array[i][j]);
            }
        }
        return complexMatrix2x2Local;
    }

    public ComplexMatrix2x2 mult(ComplexMatrix2x2 secondComplexMatrix2x2) {
        ComplexMatrix2x2 complexMatrix2x2Local = new ComplexMatrix2x2();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                complexMatrix2x2Local.array[i][j] = this.array[i][j].mult(secondComplexMatrix2x2.array[i][j]);
            }
        }
        return complexMatrix2x2Local;
    }

    public ComplexNumber det() {
        this.array[0][0].mult2(this.array[1][1]);
        this.array[1][0].mult2(this.array[0][1]);
        return array[0][0].sub(array[1][0]);
    }

    public ComplexVector2D multVector(ComplexVector2D secondComplexVector2D) {
        ComplexNumber complexNumberLocal1 = new ComplexNumber();
        ComplexNumber complexNumberLocal2 = new ComplexNumber();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((j == 0) && (i == 0)) {
                    complexNumberLocal1 = this.array[i][j].mult(secondComplexVector2D.getComplexNumber1());
                } else if ((j == 1) && (i == 0)) {
                    this.array[i][j].mult2(secondComplexVector2D.getComplexNumber2());
                    complexNumberLocal1.add2(this.array[i][j]);
                } else if ((j == 0) && (i == 1)) {
                    complexNumberLocal2 = this.array[i][j].mult(secondComplexVector2D.getComplexNumber1());
                } else {
                    this.array[i][j].mult2(secondComplexVector2D.getComplexNumber2());
                    complexNumberLocal2.add(this.array[i][j]);
                }
            }
        }
        return new ComplexVector2D(complexNumberLocal1, complexNumberLocal2);
    }
}
