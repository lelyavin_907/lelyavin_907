package ru.kpfu.itis.group907.Lelyavin.complexClasses;
import ru.kpfu.itis.group907.Lelyavin.basicClasses.*;

public class RationalComplexMatrix2x2 {
    private int n = 2;
    private RationalComplexNumber[][] array = new RationalComplexNumber[n][n];

    public RationalComplexMatrix2x2() {
        array[0][0] = new RationalComplexNumber();
        array[0][1] = new RationalComplexNumber();
        array[1][0] = new RationalComplexNumber();
        array[1][1] = new RationalComplexNumber();
    }

    public RationalComplexMatrix2x2(RationalComplexNumber rationalComplexNumber1,RationalComplexNumber rationalComplexNumber2,RationalComplexNumber rationalComplexNumber3, RationalComplexNumber rationalComplexNumber4) {
        array[0][0] = rationalComplexNumber1;
        array[0][1] = rationalComplexNumber2;
        array[1][0] = rationalComplexNumber3;
        array[1][1] = rationalComplexNumber4;
    }

    public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 secondRationalComplexMatrix2x2) {
        RationalComplexMatrix2x2 rationalComplexMatrix2x2Local = new RationalComplexMatrix2x2();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                rationalComplexMatrix2x2Local.array[i][j] = this.array[i][j].add(secondRationalComplexMatrix2x2.array[i][j]);
            }
        }
        return rationalComplexMatrix2x2Local;
    }

    public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 secondRationalComplexMatrix2x2) {
        RationalComplexMatrix2x2 rationalComplexMatrix2x2Local = new RationalComplexMatrix2x2();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                rationalComplexMatrix2x2Local.array[i][j] = this.array[i][j].mult(secondRationalComplexMatrix2x2.array[i][j]);
            }
        }
        return rationalComplexMatrix2x2Local;
    }

    public RationalComplexNumber det() {
        RationalComplexNumber rationalComplexNumberLocal1 = new RationalComplexNumber();
        RationalComplexNumber rationalComplexNumberLocal2 = new RationalComplexNumber();
        rationalComplexNumberLocal1 = this.array[0][0].mult(this.array[1][1]);
        rationalComplexNumberLocal2 = this.array[1][0].mult(this.array[0][1]);
        return rationalComplexNumberLocal1.sub(rationalComplexNumberLocal2);
    }

    public RationalComplexVector2D multVector(RationalComplexVector2D secondRationalComplexVector2D) {
        RationalComplexNumber rationalComplexNumberLocal1 = new RationalComplexNumber();
        RationalComplexNumber rationalComplexNumberLocal2 = new RationalComplexNumber();
        RationalComplexNumber rationalComplexNumberLocal3 = new RationalComplexNumber();
        RationalComplexNumber rationalComplexNumberLocal4 = new RationalComplexNumber();
        RationalComplexNumber rationalComplexNumberLocal5 = new RationalComplexNumber();
        RationalComplexNumber rationalComplexNumberLocal6 = new RationalComplexNumber();


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i == 0) && (j == 0)) {
                    rationalComplexNumberLocal1 = this.array[i][j].mult(secondRationalComplexVector2D.getRationalComplexNumber1());
                } else if ((i == 0) && (j == 1)) {
                    rationalComplexNumberLocal2 = this.array[i][j].mult(secondRationalComplexVector2D.getRationalComplexNumber2());
                    rationalComplexNumberLocal3 = rationalComplexNumberLocal1.add(rationalComplexNumberLocal2);
                } else if ((i == 1) && (j == 0)) {
                    rationalComplexNumberLocal4 = this.array[i][j].mult(secondRationalComplexVector2D.getRationalComplexNumber1());
                } else {                            // i == 1 j == 1;
                    rationalComplexNumberLocal5 = this.array[i][j].mult(secondRationalComplexVector2D.getRationalComplexNumber2());
                    rationalComplexNumberLocal6 = rationalComplexNumberLocal1.add(rationalComplexNumberLocal2);
                }
            }
        }
        return new RationalComplexVector2D(rationalComplexNumberLocal3, rationalComplexNumberLocal6);
    }
}

