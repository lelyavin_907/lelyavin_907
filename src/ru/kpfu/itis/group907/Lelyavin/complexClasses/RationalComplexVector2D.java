package ru.kpfu.itis.group907.Lelyavin.complexClasses;
import ru.kpfu.itis.group907.Lelyavin.basicClasses.*;

public class RationalComplexVector2D {
    private RationalComplexNumber rationalComplexNumber1 = new RationalComplexNumber();
    private RationalComplexNumber rationalComplexNumber2 = new RationalComplexNumber();

    public RationalComplexNumber getRationalComplexNumber1() {
        return rationalComplexNumber1;
    }

    public RationalComplexNumber getRationalComplexNumber2() {
        return rationalComplexNumber2;
    }

    public RationalComplexVector2D() {
        this.rationalComplexNumber1 = new RationalComplexNumber();
        this.rationalComplexNumber2 = new RationalComplexNumber();
    }

    public RationalComplexVector2D(RationalComplexNumber secondRationalComplexNumber1,
                            RationalComplexNumber secondRationalComplexNumber2) {
        this.rationalComplexNumber1 = secondRationalComplexNumber1;
        this.rationalComplexNumber2 = secondRationalComplexNumber2;
    }

    public RationalComplexVector2D add(RationalComplexVector2D
                                               secondRationalComplexVector2D) {
        RationalComplexVector2D rationalComplexVector2DLocal = new RationalComplexVector2D();
        rationalComplexVector2DLocal.rationalComplexNumber1 = this.rationalComplexNumber1.add(secondRationalComplexVector2D.rationalComplexNumber1);
        rationalComplexVector2DLocal.rationalComplexNumber2 = this.rationalComplexNumber2.add(secondRationalComplexVector2D.rationalComplexNumber2);
        return new RationalComplexVector2D(rationalComplexVector2DLocal.rationalComplexNumber1, rationalComplexVector2DLocal.rationalComplexNumber2);
    }

    public String toString() {
        return "(" + rationalComplexNumber1 + " , " + rationalComplexNumber2 + ")";
    }

    public RationalComplexNumber scalarProduct(RationalComplexVector2D
                                                       secondRationalComplexVector2D) {
        RationalComplexVector2D rationalComplexVector2DLocal = new RationalComplexVector2D();
        rationalComplexVector2DLocal.rationalComplexNumber1 = this.rationalComplexNumber1.mult(secondRationalComplexVector2D.rationalComplexNumber1);
        rationalComplexVector2DLocal.rationalComplexNumber2 = this.rationalComplexNumber2.mult(secondRationalComplexVector2D.rationalComplexNumber2);
        return (rationalComplexVector2DLocal.rationalComplexNumber1.add(rationalComplexVector2DLocal.rationalComplexNumber2));
    }
}
