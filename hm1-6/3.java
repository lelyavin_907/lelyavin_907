
package com.company;
import java.util.*;


public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введие коэфициенты квадратного уравнения");
        float a = in.nextFloat();
        float b = in.nextFloat();
        float c = in.nextFloat();
        double D = Math.sqrt(b*b - (4*a*c));
        System.out.println("дискриминант =" + " " + D);

        double x1 = (-b-D)/(2*a);
        double x2 = (-b+D)/(2*a);

        System.out.println("корни уравнения =" + x1 + " "+ x2);


    }
}
